#  Migrate a repository with large files to GitLab with LFS

You may have a local repository with large files. GitLab offers Large File Storage (LFS) compatibility. 
This guide was written to help migrate an existing repository to GitLab, storing large files in GitLab LFS. 
Let's get started!

##### Set up environment

- Install requirements - Git and Git LFS

    Linux: `sudo {apt, dnf} install git git-lfs`

    Windows: download and install [Git](https://git-scm.com/downloads) and [Git LFS](https://git-lfs.github.com)

- Initialize Git LFS (only needed once per user account):
    Open a terminal / command prompt and type:
    `git lfs install`

##### Let's get started!

1. Clone your repository with large files:
    ```bash
    $ git clone <your large repository>
    $ cd <your large repository>/
    ```
    
2. Track desired files:
    ```bash
    $ git lfs track "*.flac" "folder/" "whatever-else"
    ```

3. Add .gitattributes:
    ```bash
    git add .gitattributes
    ```

4. "Refresh" tracked files in git and commit:
    ```bash
    git rm --cached "*.flac" "folder/" "whatever-else"
    git add -A
    git commit -a -m "Convert to LFS"
    ```

#### Sanity Check!
At this point, a git lfs ls-files should show all your tracked files.
Now we need to push the repository to GitLab, with the tracked files pointing to GitLab LFS.

5. Add GitLab as remote origin
Edit your .git/config directly, replacing the url, for example:
    ```
    [remote "origin"]
    url = git@gitlab.com:group/project.git
    fetch = +refs/heads/*:refs/remotes/origin/
    ```
6. Convert pre-existing git objects to lfs objects
    ```
    git lfs migrate import -I "*.flac" "folder/" "whatever-else"
    ```

7. Clean up commit history
    ```
    git reflog expire --expire-unreachable=now --all
    git gc --prune=now
    ```

    **Note:** There is a chance this must be done after your git push --force

8. Force push to new GitLab remote
    ```
    git push --force
    ```
    
#### References 
- [GitLab LFS Migration Docs](https://docs.gitlab.com/ee/topics/git/lfs/migrate_to_git_lfs.html)
- [Alternate LFS Migration Method](https://github.com/git-lfs/git-lfs/wiki/Tutorial#migrating-existing-repository-data-to-lfs)
