#### get_project_ids.py ####
# gets all project ids for a gitlab group
# assumes GitLab 'private_token' is set to env. variable called $GITLAB_TOKEN

import gitlab, os

# authenticate
gl = gitlab.Gitlab('https://gitlab.com/', private_token=os.environ['GITLAB_TOKEN'])

# set gitlab group id
group_id = 55555
group = gl.groups.get(group_id, lazy=True)

#get all projects
projects = group.projects.list(include_subgroups=True, all=True)

#get all project ids
project_ids = []
for project in projects:
    project_ids.append((project.path_with_namespace, project.id))
    
print(project_ids)
